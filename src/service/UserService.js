class UserService {

  key = "users"

/**
 * get list from localStorage
 */

  getList() {
    let list = localStorage.getItem(this.key)
    if (list === null) {
      list = []
    } else {
      list = JSON.parse(list)
      return list
    }
  }

/**
 * add user to localStorage
 * @param {*} name 
 * @param {*} lastname 
 * @param {*} email 
 * @param {*} password 
 * @param {*} phone 
 */

  add(name, lastname, email, password, phone) {
    let list = localStorage.getItem(this.key)
    if (list === null) {
       list = []
    } else {
      list = JSON.parse(list)
    }
    list.push({ name, lastname, email, password, phone })
    list = JSON.stringify(list)
    localStorage.setItem(this.key, list)      
  }
}

/**
 * instatiate UserService
 * singleton pattern
 */

let instance = null
UserService.getInstance = () => {
    if (instance === null) {
        instance = new UserService()
    }
    return instance
}

export default UserService